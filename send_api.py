from flask_restful import reqparse, abort, Api, Resource
from flask import jsonify
from data.sends import Send
from data import db_session
import datetime as dt

parser = reqparse.RequestParser()
parser.add_argument('star_date', required=True)
parser.add_argument('msg', required=True)
parser.add_argument('filter', required=True)
parser.add_argument('end_time', required=True)



def abort_if_sends_not_found(send_id):
    session = db_session.create_session()
    send = session.query(Send).get(send_id)
    if not send:
        abort(404, message=f"Send {send_id} not found")


class SendsResource(Resource):
    def get(self, send_id):
        abort_if_sends_not_found(send_id)
        session = db_session.create_session()
        send = session.query(Send).get(send_id)
        return jsonify({'sends': send.to_dict(
            only=('star_date', 'msg', 'filter', 'end_time'))})

    def delete(self, send_id):
        abort_if_sends_not_found(send_id)
        session = db_session.create_session()
        send = session.query(Send).get(send_id)
        session.delete(send)
        session.commit()
        return jsonify({'success': 'OK'})


class SendsListResource(Resource):
    def get(self):
        session = db_session.create_session()
        send = session.query(Send).all()
        return jsonify({'send': [item.to_dict(
            only=('star_date', 'msg', 'filter', 'end_time')) for item in send]})

    def post(self):
        args = parser.parse_args()
        session = db_session.create_session()
        send = Send(
            star_date=dt.datetime.strptime(args['star_date'], "%Y-%m-%d %H:%M"),
            msg=args['msg'],
            filter=args['filter'],
            end_time=dt.datetime.strptime(args['end_time'], "%Y-%m-%d %H:%M")
        )
        session.add(send)
        session.commit()
        return jsonify({'success': 'OK'})
