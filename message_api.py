from flask_restful import reqparse, abort, Api, Resource
from flask import jsonify
from data.message import Message
from data import db_session
import datetime as dt


class MessagesSend(Resource):
    def get(self, send_id):
        session = db_session.create_session()
        message = session.query(Message).filter(Message.id_send == send_id).all()
        if message:
            return jsonify({'send': [item.to_dict(
                only=('datesend', 'status', 'id_client', 'id_send')) for item in message]})
        else:
            abort(404, message=f"message send id = {send_id} not found")


class MessagesStatus(Resource):
    def get(self, status):
        session = db_session.create_session()
        message = session.query(Message).filter(Message.status == bool(status)).all()
        if message:
            return jsonify({'send': [item.to_dict(
                only=('datesend', 'status', 'id_client', 'id_send')) for item in message]})
        else:
            abort(404, message=f"message with {status} not found")
