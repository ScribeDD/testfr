from flask import request
import datetime as dt
import requests
import requests

def test_sends():
    api_server = "http://127.0.0.1:5000/api/v2/send/1"
    response = requests.delete(api_server)
    print(response, response.content)

    api_server = "http://127.0.0.1:5000/api/v2/send"

    params = {
        "star_date": '2022-09-12 13:13',
        "msg": 'привет',
        "filter": "913",
        "end_time": '2022-09-12 13:13'
    }
    response = requests.post(api_server, json=params)
    print(response.content)
    response = requests.get(api_server)
    print(response.content)

def test_users():
    api_server = "http://127.0.0.1:5000/api/v2/user/1"
    response = requests.delete(api_server)
    print(response, response.content)

    api_server = "http://127.0.0.1:5000/api/v2/user"

    params = {
        "phone": '89139982312',
        "code_phone": '913',
        "tag": "free",
        "time": '+10'
    }
    response = requests.post(api_server, json=params)
    print(response, response.content)
    response = requests.get(api_server)
    print(response, response.content)

def test_message():
    api_server = "http://127.0.0.1:5000/api/v2/messagesend/3"

    response = requests.get(api_server)
    print(response.content)

    api_server = "http://127.0.0.1:5000/api/v2/messagestatus/1"

    response = requests.get(api_server)
    print(response.content)


test_message()