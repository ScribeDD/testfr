import datetime
import sqlalchemy
from sqlalchemy import orm
from .db_session import SqlAlchemyBase
from flask_login import UserMixin
from sqlalchemy_serializer import SerializerMixin

class User(SqlAlchemyBase, UserMixin, SerializerMixin):
    __tablename__ = 'users'

    id = sqlalchemy.Column(sqlalchemy.Integer,
                           primary_key=True, autoincrement=True)
    phone = sqlalchemy.Column(sqlalchemy.String, nullable=True)
    code_phone = sqlalchemy.Column(sqlalchemy.String, nullable=True)
    tag = sqlalchemy.Column(sqlalchemy.String,
                              index=True, nullable=True)
    time = sqlalchemy.Column(sqlalchemy.String,
                              index=True,  nullable=True)

    messages = orm.relation("Message", back_populates='user')