import datetime
import sqlalchemy
from sqlalchemy import orm
from .db_session import SqlAlchemyBase
from flask_login import UserMixin
from sqlalchemy_serializer import SerializerMixin

class Send(SqlAlchemyBase, UserMixin, SerializerMixin):
    __tablename__ = 'sends'

    id = sqlalchemy.Column(sqlalchemy.Integer,
                           primary_key=True, autoincrement=True)
    star_date = sqlalchemy.Column(sqlalchemy.DateTime, nullable=True)
    msg = sqlalchemy.Column(sqlalchemy.String, nullable=True)
    filter = sqlalchemy.Column(sqlalchemy.String,
                              index=True, nullable=True)
    end_time = sqlalchemy.Column(sqlalchemy.DateTime,
                              index=True,  nullable=True)

    messages = orm.relation("Message", back_populates='send')
