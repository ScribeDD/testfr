from flask_restful import reqparse, abort, Api, Resource
from flask import jsonify
from data.users import User
from data import db_session
import datetime as dt

parser = reqparse.RequestParser()
parser.add_argument('phone', required=True)
parser.add_argument('code_phone', required=True)
parser.add_argument('tag', required=True)
parser.add_argument('time', required=True)



def abort_if_users_not_found(user_id):
    session = db_session.create_session()
    user = session.query(User).get(user_id)
    if not user:
        abort(404, message=f"User {user_id} not found")


class UsersResource(Resource):
    def get(self, user_id):
        abort_if_users_not_found(user_id)
        session = db_session.create_session()
        user = session.query(User).get(user_id)
        return jsonify({'users': user.to_dict(
            only=('phone', 'code_phone', 'tag', 'time'))})

    def delete(self, user_id):
        abort_if_users_not_found(user_id)
        session = db_session.create_session()
        user = session.query(User).get(user_id)
        session.delete(user)
        session.commit()
        return jsonify({'success': 'OK'})


class UsersListResource(Resource):
    def get(self):
        session = db_session.create_session()
        user = session.query(User).all()
        return jsonify({'user': [item.to_dict(
            only=('phone', 'code_phone', 'tag', 'time')) for item in user]})

    def post(self):
        args = parser.parse_args()
        session = db_session.create_session()
        user = User(
            phone=args['phone'],
            code_phone=args['code_phone'],
            tag=args['tag'],
            time=args['time']
        )
        session.add(user)
        session.commit()
        return jsonify({'success': 'OK'})
