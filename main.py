from flask import Flask, render_template, request
from flask_restful import reqparse, abort, Api, Resource
from data import db_session
import datetime as dt
from data.sends import Send
from data.users import User
from data.message import Message
import requests
import send_api, user_api, message_api
import asyncio

#создание объекта приложения
app = Flask(__name__)
api = Api(app)


#Основаня функция запуска приложения, инициализации бд и создания связей для api
def main():
    db_session.global_init("db/blogs.db")

    api.add_resource(send_api.SendsListResource, '/api/v2/send')
    api.add_resource(send_api.SendsResource, '/api/v2/send/<int:send_id>')

    api.add_resource(user_api.UsersListResource, '/api/v2/user')
    api.add_resource(user_api.UsersResource, '/api/v2/user/<int:user_id>')

    api.add_resource(message_api.MessagesSend, '/api/v2/messagesend/<int:send_id>')
    api.add_resource(message_api.MessagesStatus, '/api/v2/messagestatus/<int:status>')

    app.run()


#функции обратоки страниц
@app.route('/')
@app.route('/index')
def index():
    db_sess = db_session.create_session()
    sends = db_sess.query(Send).all()
    return render_template('index.html', sends=sends)

@app.route("/stat")
def stat():
    db_sess = db_session.create_session()
    sends = db_sess.query(Send)
    return render_template("stat.html", sends=sends)

@app.route("/sends", methods=['GET', 'POST'])
def sends():
    if request.method == 'GET':
        return render_template("sends.html")
    elif request.method == 'POST':
        session = db_session.create_session()
        send = Send(
            star_date=(dt.datetime.strptime(request.form['firstdate'] + ' ' + request.form['firsttime'], "%Y-%m-%d %H:%M")),
            msg=request.form['msg'],
            filter=request.form['filter'],
            end_time=(dt.datetime.strptime(request.form['enddate'] + ' ' + request.form['endtime'], "%Y-%m-%d %H:%M"))
        )
        session.add(send)
        session.commit()
        return render_template("sends.html")

@app.route("/users", methods=['GET', 'POST'])
def users():
    if request.method == 'GET':
        return render_template("users.html")
    elif request.method == 'POST':
        session = db_session.create_session()
        user = User(
            phone=request.form['phone'],
            code_phone=request.form['code'],
            tag=request.form['tag'],
            time=request.form['time']
             )
        session.add(user)
        session.commit()
        return render_template("users.html")

#функция отпрвавки сообщения на внешний api
def sending():
    api_server = "https://probe.fbrq.cloud/v1/send/1"

    params = {
        "id": 1,
        "phone": 1241244,
        "text": "string"
    }
    headers = {
        "accept": "application / json:",
        "authorization":'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTI0MzI1OTAsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IkxvbmVseVNjcmliZSJ9.NxWbLT50SehwrD0f34Q7vnnrEOYP5Zwjyg8fOyED3Rs',
        "Content-Type": "application/json"
    }
    response = requests.post(api_server, headers=headers,json=params)
    print(response)


if __name__ == '__main__':
    sending()
    main()
